# base_lib

A Flutter baselib plugin

## Getting Started

This project is a starting point for a Flutter
[plug-in package](https://flutter.dev/developing-packages/), a specialized package that includes
platform-specific implementation code for Android and/or iOS.

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials, samples, guidance on
mobile development, and a full API reference.

### 引入方式：

根目录的`pubspec.yaml`下：

``` yaml
dependencies:
  flutter:
    sdk: flutter

  # The following adds the Cupertino Icons font to your application.
  # Use with the CupertinoIcons class for iOS style icons.
  baselib:
    git:
      url: https://gitee.com/chawloo_organization/flutter-base-lib.git
      ref: v1.0.1
```

### 初始化

```dart
void main() async {
  await BaseLibPlugin.init(
      isDebug: true,
      logConfig: LogConfig.obtain(
        tag: "Meng777",
        isConsolePrintLog: true,
        isFlutterPrintLog: true,
        saveLogFilePath: "${Directory.systemTemp.path}/log/log.txt",
        encryptPubKey: "Meng777",
      ),
      httpConfig: HttpConfig(),
      resConfig: ResConfig(),
      initRoute: (router) {///如果使用路由则必须传入路由表
        Routes.initRouter(router);
      }
  );
  runApp(Main());
}

class Main extends StatelessWidget{
  //·····省略部分代码·····
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //···
      onGenerateRoute: M7Route.router.generator,//添加
      home: HomePage(),//首页
    );
  }
}
```

### 包含了一下工具类：

- MK，MMKV工具类

```dart
void main() {
  var data = "Test";
  const String key = "KEY_TEST";
  MK.encode(key, data);
  var cache = MK.getString(key);
  M7Log.d("ChawLoo", "读取的缓存::$cache");
  var cacheWithDefault = MK.getString("#123123#", defaultValue: "TestDefault");
  M7Log.d("ChawLoo", "读取的缓存::$cacheWithDefault");
}
```

- 气泡消息工具类

```dart
ToastUtil.show("我是气泡消息")
```
- 日志工具类

```dart
M7Log.e(tag:"我是Tag，可选",msg: "我是日志内容哦！");
```
- Fluro路由二次封装工具类 V1.0.1新增

```dart
//基础跳转路由
M7Route.push(context, Routes.user);
//跳转路由并清空其他路由
M7Route.clearStackAndEarlier(context, Routes.main);
//带参数的路由跳转
M7Route.push(context, Routes.webview, params: {"title": title, "url": url});
WebViewPage(this._title, this._url);
```



暂时先写了这几个示例，用的最多，后续会逐步添加，已加入有生之年系列