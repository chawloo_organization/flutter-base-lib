import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';

///@description:  设备信息管理
class DeviceUtils {
  DeviceUtils._();

  ///手机型号
  static String? _model;

  ///手机品牌
  static String? _brand;

  ///系统版本
  static String? _osVersion;

  ///系统版本e.g: android 9
  static String? _osVersionStr;

  ///android or ios
  static String? _osType;

  static String _androidId = "";

  static String _identifierForVendor = "";

  static Future<String?> getBrand() async {
    await _initDeviceInfo();
    return _brand;
  }

  static Future<String?> getModel() async {
    await _initDeviceInfo();
    return _model;
  }

  ///android 9
  static Future<String?> getOsVersion() async {
    await _initDeviceInfo();
    return _osVersionStr;
  }

  ///系统类型
  static String? getOsType() {
    if (_osType == null || _osType!.isEmpty) {
      _osType = Platform.operatingSystem;
    }
    return _osType;
  }

  static Future<String> getDeviceId() async {
    await _initDeviceInfo();
    var deviceId = "";
    if (isIos()) {
      deviceId = _identifierForVendor;
    } else if (isAndroid()) {
      deviceId = _androidId;
    }
    return deviceId;
  }

  static Future<void> _initDeviceInfo() async {
    if (_osType == null || _osType!.isEmpty) {
      _osType = Platform.operatingSystem;
    }

    if (_osVersion == null || _model == null || _brand == null) {
      DeviceInfoPlugin plugin = DeviceInfoPlugin();
      if (Platform.isAndroid) {
        AndroidDeviceInfo build = await plugin.androidInfo;
        _osVersion = build.version.release;
        _model = build.model;
        _brand = build.brand;
        _androidId = build.androidId ?? "";
      } else if (Platform.isIOS) {
        IosDeviceInfo build = await plugin.iosInfo;
        _osVersion = build.systemVersion;
        _model = build.model;
        _brand = build.name;
        _identifierForVendor = build.identifierForVendor ?? "";
      } else {
        _osVersion = 'unKnow';
        _model = 'unKnow';
        _brand = 'unKnow';
        _androidId = 'unKnow';
        _identifierForVendor = 'unKnow';
      }

      _osVersionStr = '$_osType  $_osVersion';
    }
  }

  static bool isIos() {
    return Platform.isIOS;
  }

  static bool isAndroid() {
    return Platform.isAndroid;
  }
}
