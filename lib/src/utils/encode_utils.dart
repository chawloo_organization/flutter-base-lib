import 'dart:convert';

import 'package:crypto/crypto.dart';

///@date:  2021/02/24
///@author:  lixu
///@description:  常用加密算法
class EncodeUtils {
  EncodeUtils._();

  /// md5 加密
  static Future<String> generateMd5(String data) async {
    var content = const Utf8Encoder().convert(data);
    var digest = md5.convert(content);
    return digest.toString();
  }
}