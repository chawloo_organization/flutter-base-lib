import 'dart:convert';

import 'package:encrypt/encrypt.dart';

class AesUtil {
  AesUtil._();

  //必须16位
  static var _sKey = "acdefghijuklmnop";

  //必须16位
  static var _ivParameter = "acdefghijuklmnop";

  static String encrypt(String? content, String key, String iv) {
    if (content == null || content.isEmpty) {
      return "";
    }
    if (key.isNotEmpty) {
      _sKey = key;
    }
    if (iv.isNotEmpty) {
      _ivParameter = iv;
    }
    final encrypter = Encrypter(AES(Key(const Utf8Encoder().convert(_sKey))));
    final encrypted = encrypter.encrypt(content, iv: IV(const Utf8Encoder().convert(_ivParameter)));
    return encrypted.base64;
  }
}
