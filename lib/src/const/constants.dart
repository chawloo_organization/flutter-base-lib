class Constants {
  ///分页加载其起始页index
  static int initPageIndex = 1;

  ///分页加载页大小
  static int pageSize = 20;

  static const String httpStartWith = 'http';
}