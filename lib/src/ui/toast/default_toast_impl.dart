import 'package:flutter/material.dart';
import 'package:flutter_base_lib/flutter_base_lib.dart';

class DefaultToastImpl extends IToast {
  static const String _tag = 'DefaultToastImpl';
  static bool _isFirst = true;

  @override
  void show(String? text, {bool isShowLong = false}) {
    if (text != null && text.isNotEmpty) {
      if (_isFirst) {
        _isFirst = false;
      } else {
        Fluttertoast.cancel();
      }

      Fluttertoast.showToast(
        msg: text,
        toastLength: isShowLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black87,
        textColor: Colors.white,
        fontSize: 14,
      );
      M7Log.i(tag: _tag, msg: '用户可见的Toast Text:$text');
    } else {
      M7Log.w(tag: _tag, msg: "toast content is empty");
    }
  }

  @override
  void showDebug(String? text, {bool isShowLong = false}) {
    if (BaseLibPlugin.isDebug) {
      if (text != null && text.isNotEmpty) {
        if (_isFirst) {
          _isFirst = false;
        } else {
          Fluttertoast.cancel();
        }

        Fluttertoast.showToast(
          msg: text,
          toastLength: isShowLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16,
        );
      } else {
        M7Log.w(tag: _tag, msg: "showDebug toast content is empty");
      }
    }
  }
}
