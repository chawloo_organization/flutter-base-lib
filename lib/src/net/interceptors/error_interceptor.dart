import 'package:dio/dio.dart';
import 'package:flutter_base_lib/flutter_base_lib.dart';
import 'package:flutter_base_lib/src/net/model/app_exception.dart';

class ErrorInterceptor extends Interceptor {
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    AppException appException = AppException.create(err);
    M7Log.d(tag: "ChawLooHttpError", msg: appException.toString());
    err.error = appException;
    super.onError(err, handler);
  }
}
