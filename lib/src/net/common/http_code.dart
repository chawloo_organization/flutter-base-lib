class HttpCode {
  static int defaultCode = 0;
  static int success = 200;
  static int networkError = 10001; //网络错误Code
  static int fail = -1; //失败
  static int unKnowError = -99999; //未知错误
  static int cancel = -10000; //取消请求
}
