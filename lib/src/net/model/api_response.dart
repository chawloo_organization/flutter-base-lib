import 'package:flutter_base_lib/src/net/common/http_code.dart';
import 'package:flutter_base_lib/src/net/model/i_api_response.dart';

import 'app_exception.dart';

class ApiResponse<T> extends IApiResponse {
  ///http响应的数据是否是List数据结构
  bool isRespListData = false;

  ///响应对象
  T? data;

  ///响应List数据对象
  List<T>? dataList;

  ///响应完整json
  Map<String, dynamic>? json;

  ApiResponse() : super(code: HttpCode.defaultCode);

  AppException obtainException() {
    return AppException(code, msg);
  }
}
