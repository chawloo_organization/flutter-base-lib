abstract class IApiResponse {
  ///响应码
  int code;

  ///错误提示
  String? msg;

  ///错误提示
  String? message;

  IApiResponse({required this.code, this.msg, this.message});

  bool isSuccess() => code == 200 || code.toString().startsWith("200");
}
