class LogConfig {
  ///全局日志tag
  String tag;

  ///控制台是否打印日志
  bool isConsolePrintLog;

  ///是否使用flutter print方法打印,否则就是使用native打印日志（仅针对android）
  bool isFlutterPrintLog;

  ///log文件保存目录（仅针对android）该字段才有效，为null，保存到内部存储路径下
  String saveLogFilePath;

  ///日志文件保存到本地是否加密
  ///true：使用加密
  ///false：不加密
  String? encryptPubKey;

  LogConfig.obtain({
    required this.tag,
    required this.isConsolePrintLog,
    required this.isFlutterPrintLog,
    required this.saveLogFilePath,
    required this.encryptPubKey,
  });
}
