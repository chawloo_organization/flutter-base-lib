library flutter_base_lib;

export 'src/base/view/base_view.dart';
export 'src/base/view_model/base_common_view_model.dart';
export 'src/base/view_model/base_view_model.dart';
export 'src/config/bugly_config.dart';
export 'src/config/log_config.dart';
export 'src/const/constants.dart';
export 'src/interface/i_res_config.dart';
export 'src/interface/i_toast.dart';
export 'src/net/interceptors/error_interceptor.dart';
export 'src/net/interceptors/log_interceptor.dart';
export 'src/net/interceptors/retry_on_connection_change_interceptor.dart';
export 'src/net/interfaces/i_http_config.dart';
export 'src/net/model/api_response.dart';
export 'src/net/model/app_exception.dart';
export 'src/net/model/i_api_response.dart';
export 'src/net/m7_api.dart';
export 'src/ui/dialog/http_loading_dialog.dart';
export 'src/utils/aes_util.dart';
export 'src/utils/device_util.dart';
export 'src/utils/encode_utils.dart';
export 'src/utils/mk.dart';
export 'src/utils/package_utils.dart';
export 'src/utils/path_utils.dart';
export 'src/utils/permission_utils.dart';
export 'src/utils/toast_util.dart';
export 'src/base_lib_plugin.dart';
export 'src/route/m7_route.dart';

export 'package:cupertino_icons/cupertino_icons.dart';
export 'package:fluttertoast/fluttertoast.dart';
export 'package:connectivity_plus/connectivity_plus.dart';
export 'package:provider/provider.dart';
export 'package:dio/dio.dart';
export 'package:dio_cache_interceptor/dio_cache_interceptor.dart';
export 'package:dio_cache_interceptor_hive_store/dio_cache_interceptor_hive_store.dart';
export 'package:mmkv/mmkv.dart';
export 'package:device_info_plus/device_info_plus.dart';
export 'package:package_info_plus/package_info_plus.dart';
export 'package:crypto/crypto.dart';
export 'package:permission_handler/permission_handler.dart';
export 'package:encrypt/encrypt.dart' hide Key;
export 'package:url_launcher/url_launcher.dart';
export 'package:path_provider/path_provider.dart';
export 'package:webview_flutter/webview_flutter.dart';
export 'package:flutter_screenutil/flutter_screenutil.dart';
export 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
export 'package:fluwx/fluwx.dart';
export 'package:m7log/m7log.dart';
export 'package:bugly/bugly.dart';
import 'dart:async';
import 'package:flutter/services.dart';

class FlutterBaseLib {
  static const MethodChannel _channel = MethodChannel('flutter_base_lib');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
